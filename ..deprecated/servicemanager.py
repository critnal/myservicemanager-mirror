import win32serviceutil

def ServiceAction(action, service):

    if action == 'stop':
        win32serviceutil.StopService(service)
        print '%s stopped successfully' % service

    elif action == 'start':
        win32serviceutil.StartService(service)
        print '%s started successfully' % service

    elif action == 'restart':
        win32serviceutil.RestartService(service)
        print '%s restarted successfully' % service

    elif action == 'status':
        if win32serviceutil.QueryServiceStatus(service)[1] == 4:
            print '%s is running normally' % service
        else:
            print '%s is NOT running' % service

if __name__ == '__main__':
    action = 'stop'
    service = 'Apache2.2'
    ServiceAction(action, service)
