﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.ServiceProcess;
using System.Windows.Forms;

namespace MyServiceManager
{
    public partial class Form1 : Form
    {
        private static List<TextBox> textBoxes = new List<TextBox>();
        private static List<Label> labels = new List<Label>();
        private const int NUMBER_OF_SERVICES = 8;
        private static string FILE_NAME = @"services.txt";
        private static string FILE_PATH;

        public Form1()
        {
            InitializeComponent();

            textBoxes.Add(textBox1);
            textBoxes.Add(textBox2);
            textBoxes.Add(textBox3);
            textBoxes.Add(textBox4);
            textBoxes.Add(textBox5);
            textBoxes.Add(textBox6);
            textBoxes.Add(textBox7);
            textBoxes.Add(textBox8);

            // Read all the saved service names into the form text boxes
            FILE_PATH = Path.GetFullPath(FILE_NAME);
            if (!File.Exists(FILE_PATH))
                File.Create(FILE_PATH).Dispose();
            

            StreamReader reader = new StreamReader(Path.GetFullPath(FILE_NAME));
            for (int i = 0; i < NUMBER_OF_SERVICES; i++)
                textBoxes[i].Text = reader.ReadLine();
            reader.Close();

            labels.Add(label1);
            labels.Add(label2);
            labels.Add(label3);
            labels.Add(label4);
            labels.Add(label5);
            labels.Add(label6);
            labels.Add(label7);
            labels.Add(label8);

            // Check the status of every listed service
            ServiceController sC = new ServiceController();
            for (int i = 0; i < NUMBER_OF_SERVICES; i++)
            {
                if (textBoxes[i].Text != "")
                {
                    labels[i].ForeColor = Color.Crimson;
                    try
                    {
                        sC.ServiceName = textBoxes[i].Text;
                        labels[i].Text = string.Format("Status: {0}", sC.Status);
                        if (sC.Status == ServiceControllerStatus.Running)
                            labels[i].ForeColor = Color.SeaGreen;
                        
                    }
                    catch (InvalidOperationException e)
                    {
                        labels[i].Text = string.Format("Service Error");
                    }
                }
            }
        }



        private void ServiceAction(TextBox textBox, string action, Label label)
        {
            if (textBox.Text == "")
                return;

            ServiceController sC = new ServiceController();
            sC.ServiceName = textBox.Text;
            try
            {
                label.ForeColor = Color.Crimson;
                if (action == "start")
                {
                    sC.Start();
                    sC.WaitForStatus(ServiceControllerStatus.Running);
                    label.ForeColor = Color.SeaGreen;
                }
                else if (action == "stop")
                {
                    sC.Stop();
                    sC.WaitForStatus(ServiceControllerStatus.Stopped);
                }
                label.Text = string.Format("Status: {0}", sC.Status);
            }
            catch (InvalidOperationException e)
            {
                label.Text = string.Format("Service Error");
            }            
        }



        private void saveAndCloseButton_Click(object sender, EventArgs e)
        {
            File.WriteAllText(FILE_PATH, string.Empty);
            StreamWriter writer = new StreamWriter(FILE_PATH);
            for (int i = 0; i < NUMBER_OF_SERVICES; i++)
            {
                writer.WriteLine(textBoxes[i].Text);
                Console.WriteLine(textBoxes[i].Text);
            }
            writer.Close();

            Application.Exit();
        }  



        















        private void startAllButton_Click(object sender, EventArgs e)
        {
            allServices("start");
        }
        private void stopAllButton_Click(object sender, EventArgs e)
        {
            allServices("stop");
        }
        private void allServices(string action)
        {
            for (int i = 0; i < NUMBER_OF_SERVICES; i++)
            {
                ServiceAction(textBoxes[i], action, labels[i]);
            }
        }



        private void startButton1_Click(object sender, EventArgs e)
        {
            ServiceAction(textBox1, "start", label1);
            
        }
        private void stopButton1_Click(object sender, EventArgs e)
        {
            ServiceAction(textBox1, "stop", label1);
        }



        private void startButton2_Click(object sender, EventArgs e)
        {
            ServiceAction(textBox2, "start", label2);
        }
        private void stopButton2_Click(object sender, EventArgs e)
        {
            ServiceAction(textBox2, "stop", label2);
        }



        private void startButton3_Click(object sender, EventArgs e)
        {
            ServiceAction(textBox3, "start", label3);
        }
        private void stopButton3_Click(object sender, EventArgs e)
        {
            ServiceAction(textBox3, "stop", label3);
        }



        private void startButton4_Click(object sender, EventArgs e)
        {
            ServiceAction(textBox4, "start", label4);
        }
        private void stopButton4_Click(object sender, EventArgs e)
        {
            ServiceAction(textBox4, "stop", label4);
        }



        private void startButton5_Click(object sender, EventArgs e)
        {
            ServiceAction(textBox5, "start", label5);
        }
        private void stopButton5_Click(object sender, EventArgs e)
        {
            ServiceAction(textBox5, "stop", label5);
        }



        private void startButton6_Click(object sender, EventArgs e)
        {
            ServiceAction(textBox6, "start", label6);
        }
        private void stopButton6_Click(object sender, EventArgs e)
        {
            ServiceAction(textBox6, "stop", label6);
        }



        private void startButton7_Click(object sender, EventArgs e)
        {
            ServiceAction(textBox7, "start", label7);
        }
        private void stopButton7_Click(object sender, EventArgs e)
        {
            ServiceAction(textBox7, "stop", label7);
        }



        private void startButton8_Click(object sender, EventArgs e)
        {
            ServiceAction(textBox8, "start", label8);
        }
        private void stopButton8_Click(object sender, EventArgs e)
        {
            ServiceAction(textBox8, "stop", label8);
        }      
    }
}
