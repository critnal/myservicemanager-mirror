﻿namespace MyServiceManager
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.startButton1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.stopButton1 = new System.Windows.Forms.Button();
            this.stopButton2 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.startButton2 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.stopButton3 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.startButton3 = new System.Windows.Forms.Button();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.stopButton4 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.startButton4 = new System.Windows.Forms.Button();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.stopButton5 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.startButton5 = new System.Windows.Forms.Button();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.stopButton6 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.startButton6 = new System.Windows.Forms.Button();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.stopButton7 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.startButton7 = new System.Windows.Forms.Button();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.stopButton8 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.startButton8 = new System.Windows.Forms.Button();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.stopAllButton = new System.Windows.Forms.Button();
            this.startAllButton = new System.Windows.Forms.Button();
            this.saveAndCloseButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 67);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(117, 20);
            this.textBox1.TabIndex = 0;
            // 
            // startButton1
            // 
            this.startButton1.Location = new System.Drawing.Point(149, 66);
            this.startButton1.Name = "startButton1";
            this.startButton1.Size = new System.Drawing.Size(59, 23);
            this.startButton1.TabIndex = 1;
            this.startButton1.Text = "Start";
            this.startButton1.UseVisualStyleBackColor = true;
            this.startButton1.Click += new System.EventHandler(this.startButton1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(314, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 2;
            // 
            // stopButton1
            // 
            this.stopButton1.Location = new System.Drawing.Point(228, 66);
            this.stopButton1.Name = "stopButton1";
            this.stopButton1.Size = new System.Drawing.Size(59, 23);
            this.stopButton1.TabIndex = 3;
            this.stopButton1.Text = "Stop";
            this.stopButton1.UseVisualStyleBackColor = true;
            this.stopButton1.Click += new System.EventHandler(this.stopButton1_Click);
            // 
            // stopButton2
            // 
            this.stopButton2.Location = new System.Drawing.Point(228, 115);
            this.stopButton2.Name = "stopButton2";
            this.stopButton2.Size = new System.Drawing.Size(59, 23);
            this.stopButton2.TabIndex = 8;
            this.stopButton2.Text = "Stop";
            this.stopButton2.UseVisualStyleBackColor = true;
            this.stopButton2.Click += new System.EventHandler(this.stopButton2_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(314, 120);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 7;
            // 
            // startButton2
            // 
            this.startButton2.Location = new System.Drawing.Point(149, 115);
            this.startButton2.Name = "startButton2";
            this.startButton2.Size = new System.Drawing.Size(59, 23);
            this.startButton2.TabIndex = 6;
            this.startButton2.Text = "Start";
            this.startButton2.UseVisualStyleBackColor = true;
            this.startButton2.Click += new System.EventHandler(this.startButton2_Click);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(12, 116);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(117, 20);
            this.textBox2.TabIndex = 5;
            // 
            // stopButton3
            // 
            this.stopButton3.Location = new System.Drawing.Point(228, 164);
            this.stopButton3.Name = "stopButton3";
            this.stopButton3.Size = new System.Drawing.Size(59, 23);
            this.stopButton3.TabIndex = 13;
            this.stopButton3.Text = "Stop";
            this.stopButton3.UseVisualStyleBackColor = true;
            this.stopButton3.Click += new System.EventHandler(this.stopButton3_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(314, 169);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 12;
            // 
            // startButton3
            // 
            this.startButton3.Location = new System.Drawing.Point(149, 164);
            this.startButton3.Name = "startButton3";
            this.startButton3.Size = new System.Drawing.Size(59, 23);
            this.startButton3.TabIndex = 11;
            this.startButton3.Text = "Start";
            this.startButton3.UseVisualStyleBackColor = true;
            this.startButton3.Click += new System.EventHandler(this.startButton3_Click);
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(12, 165);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(117, 20);
            this.textBox3.TabIndex = 10;
            // 
            // stopButton4
            // 
            this.stopButton4.Location = new System.Drawing.Point(228, 213);
            this.stopButton4.Name = "stopButton4";
            this.stopButton4.Size = new System.Drawing.Size(59, 23);
            this.stopButton4.TabIndex = 18;
            this.stopButton4.Text = "Stop";
            this.stopButton4.UseVisualStyleBackColor = true;
            this.stopButton4.Click += new System.EventHandler(this.stopButton4_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(314, 218);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 13);
            this.label4.TabIndex = 17;
            // 
            // startButton4
            // 
            this.startButton4.Location = new System.Drawing.Point(149, 213);
            this.startButton4.Name = "startButton4";
            this.startButton4.Size = new System.Drawing.Size(59, 23);
            this.startButton4.TabIndex = 16;
            this.startButton4.Text = "Start";
            this.startButton4.UseVisualStyleBackColor = true;
            this.startButton4.Click += new System.EventHandler(this.startButton4_Click);
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(12, 214);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(117, 20);
            this.textBox4.TabIndex = 15;
            // 
            // stopButton5
            // 
            this.stopButton5.Location = new System.Drawing.Point(228, 262);
            this.stopButton5.Name = "stopButton5";
            this.stopButton5.Size = new System.Drawing.Size(59, 23);
            this.stopButton5.TabIndex = 23;
            this.stopButton5.Text = "Stop";
            this.stopButton5.UseVisualStyleBackColor = true;
            this.stopButton5.Click += new System.EventHandler(this.stopButton5_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(314, 267);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 13);
            this.label5.TabIndex = 22;
            // 
            // startButton5
            // 
            this.startButton5.Location = new System.Drawing.Point(149, 262);
            this.startButton5.Name = "startButton5";
            this.startButton5.Size = new System.Drawing.Size(59, 23);
            this.startButton5.TabIndex = 21;
            this.startButton5.Text = "Start";
            this.startButton5.UseVisualStyleBackColor = true;
            this.startButton5.Click += new System.EventHandler(this.startButton5_Click);
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(12, 263);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(117, 20);
            this.textBox5.TabIndex = 20;
            // 
            // stopButton6
            // 
            this.stopButton6.Location = new System.Drawing.Point(228, 311);
            this.stopButton6.Name = "stopButton6";
            this.stopButton6.Size = new System.Drawing.Size(59, 23);
            this.stopButton6.TabIndex = 28;
            this.stopButton6.Text = "Stop";
            this.stopButton6.UseVisualStyleBackColor = true;
            this.stopButton6.Click += new System.EventHandler(this.stopButton6_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(314, 316);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 13);
            this.label6.TabIndex = 27;
            // 
            // startButton6
            // 
            this.startButton6.Location = new System.Drawing.Point(149, 311);
            this.startButton6.Name = "startButton6";
            this.startButton6.Size = new System.Drawing.Size(59, 23);
            this.startButton6.TabIndex = 26;
            this.startButton6.Text = "Start";
            this.startButton6.UseVisualStyleBackColor = true;
            this.startButton6.Click += new System.EventHandler(this.startButton6_Click);
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(12, 312);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(117, 20);
            this.textBox6.TabIndex = 25;
            // 
            // stopButton7
            // 
            this.stopButton7.Location = new System.Drawing.Point(228, 360);
            this.stopButton7.Name = "stopButton7";
            this.stopButton7.Size = new System.Drawing.Size(59, 23);
            this.stopButton7.TabIndex = 33;
            this.stopButton7.Text = "Stop";
            this.stopButton7.UseVisualStyleBackColor = true;
            this.stopButton7.Click += new System.EventHandler(this.stopButton7_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(314, 365);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(0, 13);
            this.label7.TabIndex = 32;
            // 
            // startButton7
            // 
            this.startButton7.Location = new System.Drawing.Point(149, 360);
            this.startButton7.Name = "startButton7";
            this.startButton7.Size = new System.Drawing.Size(59, 23);
            this.startButton7.TabIndex = 31;
            this.startButton7.Text = "Start";
            this.startButton7.UseVisualStyleBackColor = true;
            this.startButton7.Click += new System.EventHandler(this.startButton7_Click);
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(12, 361);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(117, 20);
            this.textBox7.TabIndex = 30;
            // 
            // stopButton8
            // 
            this.stopButton8.Location = new System.Drawing.Point(228, 409);
            this.stopButton8.Name = "stopButton8";
            this.stopButton8.Size = new System.Drawing.Size(59, 23);
            this.stopButton8.TabIndex = 38;
            this.stopButton8.Text = "Stop";
            this.stopButton8.UseVisualStyleBackColor = true;
            this.stopButton8.Click += new System.EventHandler(this.stopButton8_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(314, 414);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(0, 13);
            this.label8.TabIndex = 37;
            // 
            // startButton8
            // 
            this.startButton8.Location = new System.Drawing.Point(149, 409);
            this.startButton8.Name = "startButton8";
            this.startButton8.Size = new System.Drawing.Size(59, 23);
            this.startButton8.TabIndex = 36;
            this.startButton8.Text = "Start";
            this.startButton8.UseVisualStyleBackColor = true;
            this.startButton8.Click += new System.EventHandler(this.startButton8_Click);
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(12, 410);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(117, 20);
            this.textBox8.TabIndex = 35;
            // 
            // stopAllButton
            // 
            this.stopAllButton.Location = new System.Drawing.Point(228, 21);
            this.stopAllButton.Name = "stopAllButton";
            this.stopAllButton.Size = new System.Drawing.Size(59, 23);
            this.stopAllButton.TabIndex = 40;
            this.stopAllButton.Text = "Stop ALL";
            this.stopAllButton.UseVisualStyleBackColor = true;
            this.stopAllButton.Click += new System.EventHandler(this.stopAllButton_Click);
            // 
            // startAllButton
            // 
            this.startAllButton.Location = new System.Drawing.Point(149, 21);
            this.startAllButton.Name = "startAllButton";
            this.startAllButton.Size = new System.Drawing.Size(59, 23);
            this.startAllButton.TabIndex = 39;
            this.startAllButton.Text = "Start ALL";
            this.startAllButton.UseVisualStyleBackColor = true;
            this.startAllButton.Click += new System.EventHandler(this.startAllButton_Click);
            // 
            // saveAndCloseButton
            // 
            this.saveAndCloseButton.Location = new System.Drawing.Point(317, 21);
            this.saveAndCloseButton.Name = "saveAndCloseButton";
            this.saveAndCloseButton.Size = new System.Drawing.Size(104, 23);
            this.saveAndCloseButton.TabIndex = 41;
            this.saveAndCloseButton.Text = "Save and Close";
            this.saveAndCloseButton.UseVisualStyleBackColor = true;
            this.saveAndCloseButton.Click += new System.EventHandler(this.saveAndCloseButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(443, 444);
            this.Controls.Add(this.saveAndCloseButton);
            this.Controls.Add(this.stopAllButton);
            this.Controls.Add(this.startAllButton);
            this.Controls.Add(this.stopButton8);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.startButton8);
            this.Controls.Add(this.textBox8);
            this.Controls.Add(this.stopButton7);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.startButton7);
            this.Controls.Add(this.textBox7);
            this.Controls.Add(this.stopButton6);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.startButton6);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.stopButton5);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.startButton5);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.stopButton4);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.startButton4);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.stopButton3);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.startButton3);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.stopButton2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.startButton2);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.stopButton1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.startButton1);
            this.Controls.Add(this.textBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button startButton1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button stopButton1;
        private System.Windows.Forms.Button stopButton2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button startButton2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button stopButton3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button startButton3;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Button stopButton4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button startButton4;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Button stopButton5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button startButton5;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Button stopButton6;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button startButton6;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Button stopButton7;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button startButton7;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Button stopButton8;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button startButton8;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Button stopAllButton;
        private System.Windows.Forms.Button startAllButton;
        private System.Windows.Forms.Button saveAndCloseButton;
    }
}

