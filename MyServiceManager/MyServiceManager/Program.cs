﻿using System;
using System.Collections.Generic;
using System.IO;
using System.ServiceProcess;
using System.Windows.Forms;

namespace MyServiceManager
{
    static class Program
    {

        
        private static Form1 form1;
        

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            form1 = new Form1();
            Application.Run(form1);
        }



        
    }
}
